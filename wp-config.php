<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'neuro_fin');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'b]kuJNwSRnaeDi<y5X_`Tx8|(L?6zQi!w.wj3{FZ.e?/)<i|9_mY*4z>JYY+C1)9');
define('SECURE_AUTH_KEY',  '[FfJrgy6Y)Z=w/i.XVL1LgPeU*H@fRl5155XKBPkkX 39GckS6a7-rC/:,{$, nX');
define('LOGGED_IN_KEY',    ':)~kr/:%pQ(UxZAg:2{{4zYqNDv<B/JFGe%G>P]<{e>LdK9?^[lrIsI_%$bY!+9-');
define('NONCE_KEY',        'F+NTK=?|a!{lmx,l-2aB!& Nz^2u{W@#up$oA.j)8$v?C1ea8w!J)(]49LHR1g b');
define('AUTH_SALT',        '>qiSmk/OoyJ6_T_J7LQP_lC3tw~x2 X(XBuN7f_8K%]r1y$WR@=$CC;1vjuS/b<k');
define('SECURE_AUTH_SALT', '.F_7,C6T`c<tsrSFEgkt-MHUYR|[HlQEyV-Hby@rsS!tv>mSh=#gET,C>QK,)}k*');
define('LOGGED_IN_SALT',   '=AGtA7/&H|590FE[i=w}<^5uytoHig6,vc{05F}`)X[s;AaQ8}Nvb]eY;KQ:]v]J');
define('NONCE_SALT',       '74ajFg5%gsW|ijEB`hd-GK88n?sUb.Z@:tYp5+{!=s^ae<?3x4rekkuv@hX@zCY0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'neuro_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
