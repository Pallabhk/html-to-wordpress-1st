<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings           = array(
  'menu_title'      => 'Theme Options',
  'menu_type'       => 'theme', // menu, submenu, options, theme, etc.
  'menu_slug'       => 'neuron-theme-option',
  'ajax_save'       => false,
  'show_reset_all'  => false,
  'framework_title' => 'Neuron Framework <small>by Pallab</small>',
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();

// ----------------------------------------
// Company Logo option section for options overview  -
// ----------------------------------------
$options[]      = array(
  'name'        => 'company_logo',
  'title'       => 'Company Logo',
  'icon'        => 'fa fa-home',

  // begin: fields
  'fields'      => array(
    array(
      'id'      => 'logos',
      'type'    => 'gallery',
      'title'   => 'Company Logo',
      'help'    =>'Upload your company logo here',
    ),

  ),// end: fields
);
// ----------------------------------------
// Home Page option section for options overview  -
// ----------------------------------------
$options[]      = array(
  'name'        => 'home_page',
  'title'       => 'Home Page',
  'icon'        => 'fa fa-home',

  // begin: fields
  'fields'      => array(
    array(
      'id'      => 'enable_homepage_promo',
      'type'    => 'switcher',
      'title'   => 'Enable promo area?',
      'default' =>true,
      'desc'    =>'If you enable Promo area title , select yes',
    ),
    array(
      'id'      => 'promo_title',
      'type'    => 'text',
      'title'   => 'Promo Title',
      'default' =>'Welcome to the Neuron Finance site',
      'desc'    =>'Type Promo Title', 
      'dependency' => array( 'enable_homepage_promo', '==', 'true' )
    ),
     array(
      'id'      => 'promo_content',
      'type'    => 'textarea',
      'title'   => 'Promo area Content',
      'default' =>'Interactively simplify 24/7 markets through 24/7 best practices. Authoritatively foster cutting-edge manufactured products and distinctive.',
      'desc'    =>'Type Promo area Content',
      'dependency' => array( 'enable_homepage_promo', '==', 'true' ),
    ),
      array(
      'id'      => 'enable_home_content_promo',
      'type'    => 'switcher',
      'title'   => 'Enable Home content area?',
      'default' =>true,
      'desc'    =>'If you Enable Home content title , select yes',
    ),
      array(
      'id'      => 'home_content_title',
      'type'    => 'text',
      'title'   => 'Home area title',
      'default' =>'A Finance Agency Crafting Beautiful & Engaging Online Experiences',
      'desc'    =>'Type Home area title',
      'dependency' => array( 'enable_home_content_promo', '==', 'true' ),

    ),
       array(
      'id'      => 'home_content',
      'type'    => 'textarea',
      'title'   => 'Home area Content',
      'desc'    =>'Type Promo area Content',
      'dependency' => array( 'enable_home_content_promo', '==', 'true' ),

    ),
        array(
      'id'      => 'home_content_image',
      'type'    => 'image',
      'title'   => 'Home area Content Image',
      'dependency' => array( 'enable_home_content_promo', '==', 'true' ),

    ),
  ), // end: fields
);

// ----------------------------------------
// About Page option section for options overview  -
// ----------------------------------------
$options[]      = array(
  'name'        => 'about_us',
  'title'       => 'About Us',
  'icon'        => 'fa fa-star',

  // begin: fields
  'fields'      => array(
     array(
      'id'              => 'faqs',
      'type'            => 'group',
      'title'           => 'FAQs',
      'button_title'    =>'Add New',
      'accordian_title' =>'Add new FAQs',
      'fields'          => array(
            array(
              'id'    => 'title',
              'type'  => 'text',
              'title' => 'FAQs title',
            ),
            array(
              'id'    => 'content',
              'type'  => 'textarea',
              'title' => 'FAQ content',
            ),
    ),
     

  ), // end: fields
      array(
      'id'      => 'enable_about_us',
      'type'    => 'switcher',
      'title'   => 'Enable About us area?',
      'default' =>true,
      'desc'    =>'If you enable About us area , select yes',
    ),
    array(
      'id'      => 'about_us_title',
      'type'    => 'text',
      'title'   => 'About Title',
      'default' =>'Welcome to the Neuron Finance site',
      'desc'    =>'Type About Title', 
      'dependency' => array( 'enable_about_us', '==', 'true' )
    ),
     array(
      'id'      => 'about_us_content',
      'type'    => 'textarea',
      'title'   => 'About area Content',
      'desc'    =>'Type About area Content',
      'dependency' => array( 'enable_about_us', '==', 'true' ),
    ),
),
);

CSFramework::instance( $settings, $options );
