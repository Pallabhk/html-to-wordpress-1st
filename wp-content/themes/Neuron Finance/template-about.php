<?php
/*
Template Name: About Template

*/

$enable_about_title =cs_get_option('enable_about_title');

?>

<?php get_header(); ?>

	
	<?php while(have_posts()):
			the_post();
	?>
	<!-- ::::::::::::::::::::: Page Title Section:::::::::::::::::::::::::: -->
		<section <?php if(has_post_thumbnail()): ?>style="background-image: url(<?php the_post_thumbnail_url('large');?>);"<?php endif;?> class="page-title">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<!-- breadcrumb content -->
						<div class="page-breadcrumbd">
							<h2><?php the_title();?></h2>
							<p><a href="<?php echo site_url();?>">Home</a> / <a href=""></a> <?php the_title();?></p>
						</div><!-- end breadcrumb content -->
					</div>
				</div>
			</div>
		</section><!-- end breadcrumb -->
	
		<!-- ::::::::::::::::::::: Block Section:::::::::::::::::::::::::: -->
		<section class="block about-us-block section-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<!-- block text -->
						<div class="block-text">
							<?php the_content();?>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; ?>
	
	<?php if ($enable_homepage_promo ==true) {
			get_template_part('content/promo');
		} 
		?>

	<section class="accordian-section section-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="accorian-item">
							<!-- <pre><?php echo var_dump(cs_get_option('faqs'));?></pre>  -->

							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<?php
							$faq_no =0;
							 $faqs  =cs_get_option('faqs');
							 foreach($faqs as $faq):
							 	$faq_no++;

							 	if ($faq_no ==1) {
							 		$aria_expanded= 'true';
							 		$in_class ='in';
							 	}else{
							 		$aria_expanded='false';
							 		$in_class= '';
							 	}

							?>
								<!-- accordian item-<?php echo $faq_no;?> -->
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading<?php echo $faq_no;?>">
										<h4 class="panel-title">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $faq_no;?>" aria-expanded="<?php echo $aria_expanded;?>" aria-controls="collapse<?php echo $faq_no;?>">
										<?php echo $faq['title'];?>
										</a>
										</h4>
									</div>
									<div id="collapse<?php echo $faq_no;?>" class="panel-collapse collapse <?php echo $in_class;?>" role="tabpanel" aria-labelledby="heading<?php echo $faq_no;?>">
										<div class="panel-body">
											<?php echo wpautop($faq['content']);?>
										</div>
									</div>
								</div>
								
							<?php endforeach;?>
							</div>
						</div>
					</div>
					<?php 
					$enable_about_us   =cs_get_option('enable_about_us');
					$about_us_title    =cs_get_option('about_us_title');
					$about_us_content  =cs_get_option('about_us_content');

					?>
					<?php if($enable_about_us ==1):?>
					<div class="col-md-6">
						<!-- accordian right text block -->
						<div class="accordian-right-content">
							<h2><?php echo $about_us_title;?></h2>
							<?php echo wpautop($about_us_content);?>
						</div>
					</div>
					<?php endif?>
				</div>
			</div>
		</section>

<?php get_footer();?>