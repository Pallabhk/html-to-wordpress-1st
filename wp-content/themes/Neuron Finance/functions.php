<?php
	/*Add Theme Support*/

	function neuro_theme_supports(){
		//loading theme text Domain
		load_theme_textdomain( 'neuro-theme', get_template_directory() . '/languages' );
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );
		//Adding Support for Automatic title tag
		add_theme_support( 'title-tag' );
		//Adding Support for thumbnails
		add_theme_support( 'post-thumbnails' );
		//Adding custom Post Image
		add_image_size('neuron-blog-img', 740,520,true);
		
		//Menu Register
		register_nav_menus( array(
			'menu-1'=>esc_html( 'Primary', 'neouron-fin' ),
		) );
		 //HTML5 Support
		 add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
     // Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
		// Add theme support for custom Logo
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

	}
	add_action( 'after_setup_theme','neuro_theme_supports' );
	add_filter( 'widget_text', 'do_shortcode');

	/* Add css and js file*/

	function neuro_css_js(){
		/*Css file*/
		wp_enqueue_style('animate-css', get_template_directory_uri().'/assets/css/animate.min.css', 'all' );
		wp_enqueue_style('font-awesome', get_template_directory_uri().'/assets/fonts/font-awesome/css/font-awesome.min.css','all' );
		wp_enqueue_style('carousel-css', get_template_directory_uri().'/assets/css/owl.carousel.min.css','all' );
		wp_enqueue_style('bootsnav-css', get_template_directory_uri().'/assets/css/bootsnav.css','all' );
		wp_enqueue_style('bootstrap-css', get_template_directory_uri().'/assets/bootstrap/css/bootstrap.min.css' ,'all');
		wp_enqueue_style('animate-css', get_template_directory_uri().'/assets/css/animate.min.css', 'all' );
		wp_enqueue_style( 'main-css', get_stylesheet_uri());

		/*Js file*/

		wp_enqueue_script('jquery');
		wp_enqueue_script( 'bootstrap-js',get_template_directory_uri().'/assets/bootstrap/js/bootstrap.min.js','all' );
		wp_enqueue_script( 'carousel-js',get_template_directory_uri().'/assets/js/owl.carousel.min.js','all' );
		wp_enqueue_script( 'wow-js',get_template_directory_uri().'/assets/js/wow.min.js','all' );
		// wp_enqueue_script( 'ajaxchimp-js',get_template_directory_uri().'/assets/js/ajaxchimp.js','all' );
		// wp_enqueue_script( 'ajaxchimp-config-js',get_template_directory_uri().'/assets/js/ajaxchimp-config.js','all' );
		wp_enqueue_script( 'script-js',get_template_directory_uri().'/assets/js/script.js','all' );

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	}
add_action('wp_enqueue_scripts','neuro_css_js');


/*Custom Post*/
	
	function neuro_custom_post(){

		/*Slider Option*/
		register_post_type( 'slider',array(
			'labels' =>array(
				'name'       =>'neuro_slider',
				'menu_name'  =>'Slider menu',
				'all_items'  =>'Slider all item',
				'add_new'    =>'Add new Slide',
				'add_new_item'=>'Add new slide item'
			),
			
			'public'  =>true,
			'supports'=>array(
				'title','editor','thumbnail','revisions','custom-fields','page-attributes'
			)
		));

		/*Frature Option*/
		register_post_type( 'feature',array(
			'labels' =>array(
				'name'       =>'neuro_feature',
				'menu_name'  =>'Feature menu',
				'all_items'  =>'Feature all item',
				'add_new'    =>'Add new feature',
				'add_new_item'=>'Add new feature item'
			),
			
			'public'  =>true,
			'supports'=>array(
				'title','editor','thumbnail','revisions' ,'page-attributes'
			)
		));
		
		/*Service Option*/

		register_post_type( 'Service',array(
			'labels' =>array(
				'name'       =>'neuro_service',
				'menu_name'  =>'Service menu',
				'all_items'  =>'Service all item',
				'add_new'    =>'Add new Service',
				'add_new_item'=>'Add new Service item'
			),
			
			'public'  =>true,
			'supports'=>array(
				'title','editor','thumbnail','revisions','custom-fields','page-attributes'
			)
		));

		/*Poftfolio  Option*/

		register_post_type( 'work',array(
			'labels' =>array(
				'name'       =>'neuro_Work',
				'menu_name'  =>'Work menu',
				'all_items'  =>'Work all item',
				'add_new'    =>'Add new Work',
				'add_new_item'=>'Add new Work item'
			),
			
			'public'  =>true,
			'supports'=>array(
				'title','editor','thumbnail','revisions','custom-fields','page-attributes'
			)
		));

	}
	add_action('init' ,'neuro_custom_post');

	/*Register Widget*/
	function neuro_widgets_init() {

		/*First Footer*/
	register_sidebar( array(
		'name'          => esc_html__( 'Footer-1', 'neuro' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'neuro' ),
		'before_widget' => '<div id="%1$s" class="footer-widget" %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	/*Footer Menu*/
		register_sidebar( array(
		'name'          => esc_html__( 'Footer-2', 'neuro' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'neuro' ),
		'before_widget' => '<div id="%1$s" class="footer-widget" %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer-3', 'neuro' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'neuro' ),
		'before_widget' => '<div id="%1$s" class="footer-widget" %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		
		'name'          => esc_html__( 'Footer-4', 'neuro' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add Footer-4 widgets here.', 'neuro' ),
		'before_widget' => '<div id="%1$s" class="footer-widget" %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );


}
add_action( 'widgets_init', 'neuro_widgets_init' );


/*Leatest Post*/
    function thumbpost_list_shortcode($atts) {
    extract( shortcode_atts( array(
        'count' => 3,
              ), $atts) );
     
    $q = new WP_Query(
        array('posts_per_page' => $count, 'post_type' => 'post')
            );      
         
    $list = '<ul>';
    while($q->have_posts()) : $q->the_post();
        $idd = get_the_ID();
        $list .= '
        	<li>
				'.get_the_post_thumbnail($idd,'thumbnail').'
				<p><a href="'.get_permalink().'">'.get_the_title().'</a></p>
				<span>'.get_the_date('d F Y', $idd) .'</span>
			</li>
        ';        
    endwhile;
    $list.= '</ul>';
    wp_reset_query();
    return $list;
}
add_shortcode('thumb_posts', 'thumbpost_list_shortcode');

include_once('inc/codestar-framework-1.0.2/cs-framework.php');

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function neuron_categorized_blog() {
	$category_count = get_transient( 'neuron_categories' );

	if ( false === $category_count ) {
		// Create an array of all the categories that are attached to posts.
		$categories = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$category_count = count( $categories );

		set_transient( 'neuron_categories', $category_count );
	}

	// Allow viewing case of 0 or 1 categories in post preview.
	if ( is_preview() ) {
		return true;
	}

	return $category_count > 1;
}



if ( ! function_exists( 'neuron_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function neuron_entry_footer() {

	/* translators: used between list items, there is a space after the comma */
	$separate_meta = __( ', ', 'neuron' );

	// Get Categories for posts.
	$categories_list = get_the_category_list( $separate_meta );

	

	// We don't want to output .entry-footer if it will be empty, so make sure its not.
	if ( ( neuron_categorized_blog() && $categories_list )   || get_edit_post_link() ) {

		echo '<footer class="entry-footer">';

			if ( 'post' === get_post_type() ) {
				if ( ( $categories_list && neuron_categorized_blog() ) || $tags_list ) {
					echo '<span class="cat-tags-links">';

						// Make sure there's more than one category before displaying.
						if ( $categories_list && neuron_categorized_blog() ) {
							echo '<span class="cat-links">'  . $categories_list . '</span>';
						}


					echo '</span>';
				}
			}

			
	}
}
endif;


?>
